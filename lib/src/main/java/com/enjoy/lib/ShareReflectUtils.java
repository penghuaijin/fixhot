package com.enjoy.lib;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import dalvik.system.PathClassLoader;

/**
 * @author penghuaijin
 * @date 2019-09-26 16:54
 * @Description: (反射工具类)
 */
public class ShareReflectUtils {

    /**
     * 反射获取一个熟悉
     *
     * @param instance
     * @param name
     * @return
     */
    public static Field getField(Object instance, String name) throws NoSuchFieldException {
        for (Class<?> cls = instance.getClass(); cls != null; cls = cls.getSuperclass()) {
            try {

                Field declaredField = cls.getDeclaredField(name);
                declaredField.setAccessible(true);


                return declaredField;
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
        }

        throw new NoSuchFieldException("Field:" + name + "not found in " + instance.getClass());


    }


    /**
     * 反射获取一个方法
     *
     * @param instance
     * @param name
     * @return
     */
    public static Method getMethod(Object instance, String name,Class<?>... parameterTypes) throws NoSuchMethodException {
        for (Class<?> cls = instance.getClass(); cls != null; cls = cls.getSuperclass()) {
            try {

                Method declaredMethod = cls.getDeclaredMethod(name,parameterTypes);
                declaredMethod.setAccessible(true);

                return declaredMethod;
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
        }

        throw new NoSuchMethodException("Method:" + name + "not found in " + instance.getClass());


    }

}
