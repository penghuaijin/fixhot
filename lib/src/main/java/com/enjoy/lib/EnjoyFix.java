package com.enjoy.lib;

import android.content.Context;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import dalvik.system.PathClassLoader;

/**
 * @author penghuaijin
 * @date 2019-09-26 17:08
 * @Description: ()
 */
public class EnjoyFix {
    public static void installPatch(Context context, String path) {
        File cacheDir = context.getCacheDir();

        //PathClassLoader
        ClassLoader classLoader = context.getClassLoader();

        try {


            Field pathListField = SharedReflectUtils2.getField(classLoader, "pathList");
            //dexPathList
            Object pathList = pathListField.get(classLoader);

            Field dexElementsField = SharedReflectUtils2.getField(pathList, "dexElements");
            //Elements[]
            Object[] oldElements = (Object[]) dexElementsField.get(pathList);

            //执行makePathElements让我们的补丁包变成一个Elements

            Method makePathElements = SharedReflectUtils2.getMethod(pathList,
                    "makePathElements",
//                    "makeDexElements",
                    List.class
                    , File.class,
                    List.class);

            ArrayList<IOException> suppressedException = new ArrayList<>();

            ArrayList<File> files = new ArrayList<>();
            files.add(new File(path));
            Object[] newElements = (Object[]) makePathElements.invoke(null, files, cacheDir, suppressedException);


            Object[] replaceElement = (Object[]) Array.newInstance(oldElements.getClass().getComponentType(), oldElements.length + newElements.length);

            System.arraycopy(newElements, 0, replaceElement, 0, newElements.length);
            System.arraycopy(oldElements, 0, replaceElement, newElements.length, oldElements.length);

            //替换
            dexElementsField.set(pathList, replaceElement);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 执行修复！
     */
    public static void installPatch2(Context context, String patch) {
        // data/data/包名/files
        File cacheDir = context.getCacheDir();

        //PathClassLoader
        ClassLoader classLoader = context.getClassLoader();

        try {
            Field pathListField = ShareReflectUtils.getField(classLoader, "pathList");
            //DexPathList
            Object pathList = pathListField.get(classLoader);

            //Element[]
            Field dexElementsField = ShareReflectUtils.getField(pathList, "dexElements");
            Object[] oldElement = (Object[]) dexElementsField.get(pathList);

            File file = new File(patch);
            ArrayList<File> files = new ArrayList<>();
            files.add(file);
            //执行makepathelement 让我们的补丁包变成一个 Element[]
            Method makePathElements = ShareReflectUtils.getMethod(pathList, "makeDexElements",
                    List.class, File.class, List.class);
            ArrayList<IOException> suppressedExceptions = new ArrayList<IOException>();
            Object[] newElement = (Object[]) makePathElements.invoke(null, files, cacheDir, suppressedExceptions);
            //要替换系统原本的Element数组的新数组
            Object[] replaceElement = (Object[]) Array.newInstance(oldElement.getClass().getComponentType(), oldElement.length + newElement.length);

            //补丁包先进去
            System.arraycopy(newElement,0,replaceElement,0,newElement.length);
            System.arraycopy(oldElement,0,replaceElement,newElement.length,oldElement.length);

            //替换！
            dexElementsField.set(pathList,replaceElement);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
