package com.enjoy.lib;

import android.app.Application;
import android.content.Context;
import android.os.Environment;

import java.io.File;


/**
 * @author penghuaijin
 * @date 2019-09-26 16:50
 * @Description: ()
 */
public class App extends Application {
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        String path = Environment.getExternalStorageDirectory().getPath() + File.separator + "patch.jar";
        EnjoyFix.installPatch(this, path);

//        EnjoyFix.installPatch(this,  "/sdcard/patch.jar");
    }
}

